let indexValue = 0;
let intervalId;

function SlideShow() {
  intervalId = setInterval(function () {
    let x;
    const img = document.querySelectorAll("img");
    for (x = 0; x < img.length; x++) {
      img[x].style.display = "none";
    }
    indexValue++;
    if (indexValue > img.length) {
      indexValue = 1;
    }
    img[indexValue - 1].style.display = "block";
  }, 3000);
}

SlideShow();

const stopButton = document.getElementById("stop-btn");
const resumeButton = document.getElementById("resume-btn");

stopButton.addEventListener("click", function () {
  clearInterval(intervalId);
});

resumeButton.addEventListener("click", function () {
  SlideShow();
});